package controller

import (
	"api-users/app/auths/dto"
	services "api-users/app/auths/service"
	"api-users/utils/response"
	"net/http"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

func AuthLoginController(c *fiber.Ctx) error {
	authLogin := new(dto.AuthDTOLoginJSON)
	if err := c.BodyParser(authLogin); err != nil {
		return c.Status(http.StatusBadRequest).JSON(response.ResponseError(http.StatusBadRequest, response.MsgFailed, err.Error()))
	}

	validate := validator.New()
	if err := validate.Struct(authLogin); err != nil {
		return c.Status(http.StatusBadRequest).JSON(response.ResponseError(http.StatusBadRequest, response.MsgFailed, err.Error()))
	}

	user, err := services.AuthLoginService(authLogin)
	if err != nil {
		return c.Status(http.StatusBadRequest).JSON(response.ResponseError(http.StatusBadRequest, response.MsgFailed, err.Error()))
	}

	return c.Status(http.StatusOK).JSON(response.ResponsSuccess(http.StatusOK, response.MsgSuccess, user))
}

func AuthRegisterController(c *fiber.Ctx) error {
	authRegister := new(dto.AuthDTORegisterJSON)
	if err := c.BodyParser(authRegister); err != nil {
		return c.Status(http.StatusBadRequest).JSON(response.ResponseError(http.StatusBadRequest, response.MsgFailed, err.Error()))
	}

	validate := validator.New()
	if err := validate.Struct(authRegister); err != nil {
		return c.Status(http.StatusBadRequest).JSON(response.ResponseError(http.StatusBadRequest, response.MsgFailed, err.Error()))
	}

	user, err := services.AuthRegisterService(authRegister)
	if err != nil {
		return c.Status(http.StatusBadRequest).JSON(response.ResponseError(http.StatusBadRequest, response.MsgFailed, err.Error()))
	}

	return c.Status(http.StatusOK).JSON(response.ResponsSuccess(http.StatusOK, response.MsgSuccess, user))
}
