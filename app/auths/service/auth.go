package services

import (
	"api-users/app/auths/dto"
	usersModel "api-users/app/users/model"
	"api-users/config"
	"api-users/pkg/db"
	usersRepository "api-users/repository/users"
	"database/sql"
	"fmt"
	"strings"
	"time"

	"github.com/golang-jwt/jwt"

	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
)

// AuthLoginService handles the login authentication logic
func AuthLoginService(authModel *dto.AuthDTOLoginJSON) (*dto.UsersAuthDTO, error) {
	usersRepo := usersRepository.UsersInitiateRepository(db.StartDB())

	user, err := usersRepo.GetUserByEmailRepository(authModel.Email)
	if err != nil {
		return nil, fmt.Errorf("failed to get user by email: %w", err)
	}

	if !compareHashAndPassword(user.Password, authModel.Password) {
		return nil, fmt.Errorf("user or password not match")
	}

	userDetails, err := usersRepo.GetUserByIDUsersDetailRepository(user.ID)
	if err != nil {
		return nil, fmt.Errorf("failed to get user details: %w", err)
	}

	accessToken, err := generateToken(user.ID, time.Hour*24, userDetails)
	if err != nil {
		return nil, fmt.Errorf("failed to generate access token: %w", err)
	}

	refreshToken, err := generateToken(user.ID, time.Hour*365, userDetails)
	if err != nil {
		return nil, fmt.Errorf("failed to generate refresh token: %w", err)
	}

	usersJson := dto.UserAuthResponseToDTO(user, userDetails, accessToken, refreshToken)
	return usersJson, nil
}

func AuthRegisterService(authModel *dto.AuthDTORegisterJSON) (*dto.UsersAuthDTO, error) {
	usersRepo := usersRepository.UsersInitiateRepository(db.StartDB())

	user, _ := usersRepo.GetUserByEmailRepository(authModel.Email)

	if user != nil {
		return nil, fmt.Errorf("email already exists, try with another email")
	}

	user, _ = usersRepo.GetUserByPhoneRepository(authModel.Phone)

	if user != nil {
		return nil, fmt.Errorf("phone already exists, try with another phone")
	}

	hashedPassword, err := hashPassword([]byte(authModel.Password))
	if err != nil {
		return nil, fmt.Errorf("failed to hash password: %w", err)
	}

	// Construct user model
	user = &usersModel.UsersModel{
		ID:        uuid.New().String(),
		Email:     strings.ToLower(authModel.Email),
		Phone:     authModel.Phone,
		Status:    1,
		Type:      1,
		Active:    true,
		Password:  hashedPassword,
		CreatedAt: sql.NullTime{Time: time.Now(), Valid: true},
	}

	// Construct user details model
	userDetails := &usersModel.UsersDetailsModel{
		ID:        uuid.New().String(),
		UsersID:   user.ID,
		FName:     strings.ToLower(authModel.FName),
		LName:     strings.ToLower(authModel.LName),
		Dob:       sql.NullTime{Time: time.Time{}, Valid: false},
		Status:    1,
		Type:      1,
		Active:    true,
		CreatedAt: sql.NullTime{Time: time.Now(), Valid: true},
	}

	// Create user and user details
	err = usersRepo.CreateUsersRepository(user, userDetails)
	if err != nil {
		return nil, fmt.Errorf("failed to create user: %w", err)
	}

	accessToken, err := generateToken(user.ID, time.Hour*24, userDetails)
	if err != nil {
		return nil, fmt.Errorf("failed to generate access token: %w", err)
	}

	refreshToken, err := generateToken(user.ID, time.Hour*365, userDetails)
	if err != nil {
		return nil, fmt.Errorf("failed to generate refresh token: %w", err)
	}

	usersJson := dto.UserAuthResponseToDTO(user, userDetails, accessToken, refreshToken)
	return usersJson, nil
}

// compareHashAndPassword compares the hash with the provided password
func compareHashAndPassword(hash, password string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

// hashPassword generates a hash from the provided password
func hashPassword(password []byte) (string, error) {
	// Generate hash from password
	hashBytes, err := bcrypt.GenerateFromPassword(password, bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}
	return string(hashBytes), nil
}

// generateToken creates a JWT token for the given user ID and expiration time
func generateToken(userID string, expire time.Duration, userDetail ...interface{}) (string, error) {

	claims := jwt.MapClaims{
		"users_id": userID,
		// "users":    userDetail,
		"exp":   time.Now().Add(expire).Unix(),
		"admin": false,
		// "users_details": userDetail,
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	// Generate encoded token
	tokenString, err := token.SignedString([]byte(config.ConfigApp().JwtSecretKey))
	if err != nil {
		return "", err
	}

	return tokenString, nil
}
