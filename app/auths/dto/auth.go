package dto

import (
	"api-users/app/users/model"
	"database/sql"
)

type UsersDTO struct {
	ID        string       `json:"id"`
	Email     string       `json:"email"`
	Phone     string       `json:"phone"`
	Status    int          `json:"status"`
	Type      int          `json:"type"`
	Active    bool         `json:"active"`
	CreatedAt sql.NullTime `json:"createdAt"`
	UpdatedAt sql.NullTime `json:"updatedAt"`
	DeletedAt sql.NullTime `json:"deletedAt"`
}

type UsersAuthDTO struct {
	ID        string                `json:"id"`
	Email     string                `json:"email"`
	Phone     string                `json:"phone"`
	Status    int                   `json:"status"`
	Type      int                   `json:"type"`
	Active    bool                  `json:"active"`
	CreatedAt sql.NullTime          `json:"createdAt"`
	UpdatedAt sql.NullTime          `json:"updatedAt"`
	DeletedAt sql.NullTime          `json:"deletedAt"`
	Detail    UsersDetailsDTO       `json:"detail"`
	Token     UserTokensResponseDTO `json:"token"`
}

type UsersDetailsDTO struct {
	ID        string       `json:"id"`
	UsersID   string       `json:"users_id"`
	FName     string       `json:"fname"`
	LName     string       `json:"lname"`
	Dob       sql.NullTime `json:"dob"`
	Status    int          `json:"status"`
	Type      int          `json:"type"`
	Active    bool         `json:"active"`
	CreatedAt sql.NullTime `json:"createdAt"`
	UpdatedAt sql.NullTime `json:"updatedAt"`
	DeletedAt sql.NullTime `json:"deletedAt"`
}

type UserTokensResponseDTO struct {
	Tokens  string `json:"token"`
	Refresh string `json:"refresh"`
}

func UserResponseToDTO(users *model.UsersModel) *UsersDTO {
	return &UsersDTO{
		ID:        users.ID,
		Email:     users.Email,
		Phone:     users.Phone,
		Status:    users.Status,
		Type:      users.Type,
		Active:    users.Active,
		CreatedAt: users.CreatedAt,
		UpdatedAt: users.UpdatedAt,
		DeletedAt: users.DeletedAt,
	}
}

func UserAuthResponseToDTO(users *model.UsersModel, usersDetails *model.UsersDetailsModel, tokens string, refresh string) *UsersAuthDTO {
	return &UsersAuthDTO{
		ID:        users.ID,
		Email:     users.Email,
		Phone:     users.Phone,
		Status:    users.Status,
		Type:      users.Type,
		Active:    users.Active,
		CreatedAt: users.CreatedAt,
		UpdatedAt: users.UpdatedAt,
		DeletedAt: users.DeletedAt,
		Detail: UsersDetailsDTO{
			ID:        usersDetails.ID,
			UsersID:   usersDetails.UsersID,
			FName:     usersDetails.FName,
			LName:     usersDetails.LName,
			Dob:       usersDetails.Dob,
			Status:    usersDetails.Status,
			Type:      usersDetails.Type,
			Active:    usersDetails.Active,
			CreatedAt: usersDetails.CreatedAt,
			UpdatedAt: usersDetails.UpdatedAt,
			DeletedAt: usersDetails.DeletedAt,
		},
		Token: UserTokensResponseDTO{
			Tokens:  tokens,
			Refresh: refresh,
		},
	}
}

type AuthDTOLoginJSON struct {
	Email    string `json:"email" validate:"required,email"`
	Password string `json:"password" validate:"required,min=8"`
}
type AuthDTOLogin struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type AuthDTORegisterJSON struct {
	Email    string `json:"email" validate:"required,email"`
	Phone    string `json:"phone" validate:"required,min=11,max=13"`
	Password string `json:"password" validate:"required,min=8"`
	FName    string `json:"fname" validate:"required,min=3"`
	LName    string `json:"lname" validate:"required,min=3"`
}
