package dto

import "math"

type PaginationDTOJSON struct {
	Pagination Pagination `json:"pages"`
	// Result     []*AddressDTOToJSON `json:"result"`
}

type Pagination struct {
	Page     int `json:"page"`
	Size     int `json:"size"`
	PagePrev int `json:"pagePrev"`
	PageNext int `json:"pageNext"`
	Total    int `json:"total"`
}

// CalculatePagePrev calculates the previous page number.
func (p *Pagination) CalculatePagePrev() {
	p.PagePrev = int(math.Max(float64(p.Page-1), 1))
}

// CalculatePageNext calculates the next page number.
func (p *Pagination) CalculatePageNext() {
	p.PageNext = int(math.Min(float64(p.Page+1), math.Ceil(float64(p.Total)/float64(p.Size))))
}

func GetPagination(page int, size int, total int) *Pagination {
	// p := new(PaginationDTOJSON)
	p := new(Pagination)
	p.Page = page
	p.Size = size
	p.Total = total
	p.CalculatePagePrev()
	p.CalculatePageNext()
	return p
}
