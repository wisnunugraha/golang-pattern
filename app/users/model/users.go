package model

import "database/sql"

type UsersModel struct {
	ID        string       `db:"id"`
	Email     string       `db:"email"`
	Phone     string       `db:"phone"`
	Password  string       `db:"password"`
	Status    int          `db:"status"`
	Type      int          `db:"type"`
	Active    bool         `db:"active"`
	CreatedAt sql.NullTime `db:"created_at"`
	UpdatedAt sql.NullTime `db:"updated_at"`
	DeletedAt sql.NullTime `db:"deleted_at"`
}

type UsersDetailsModel struct {
	ID        string       `db:"id"`
	UsersID   string       `db:"users_id"`
	FName     string       `db:"fname"`
	LName     string       `db:"lname"`
	Dob       sql.NullTime `db:"dob"`
	Status    int          `db:"status"`
	Type      int          `db:"type"`
	Active    bool         `db:"active"`
	CreatedAt sql.NullTime `db:"created_at"`
	UpdatedAt sql.NullTime `db:"updated_at"`
	DeletedAt sql.NullTime `db:"deleted_at"`
}

type UsersByEmailSecreteModel struct {
	ID       string `db:"id"`
	Email    string `db:"email"`
	Password string `db:"password"`
}
