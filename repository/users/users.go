package users

import (
	userModel "api-users/app/users/model"
	"api-users/pkg/db"
	usersQueries "api-users/repository/queries/users"
	"context"
	"database/sql"
	"fmt"
)

type UsersRepository struct {
	db *db.DB
}

func UsersInitiateRepository(db *db.DB) UsersRepositoryInterface {
	return &UsersRepository{db}
}

func (repo *UsersRepository) GetUserByEmailRepository(email string) (*userModel.UsersModel, error) {

	user := userModel.UsersModel{}
	err := repo.db.DB.Get(&user, usersQueries.SelectByEmailSecret, email)

	if err != nil {
		if err == sql.ErrNoRows {
			// Handle the case where no rows were found for the given email
			return nil, fmt.Errorf("user not found with email %s", email)
		}
		// Handle other errors
		return nil, err
	}
	return &user, nil
}

func (repo *UsersRepository) GetUserByPhoneRepository(phone string) (*userModel.UsersModel, error) {

	user := userModel.UsersModel{}
	err := repo.db.DB.Get(&user, usersQueries.SelectByPhoneSecret, phone)

	if err != nil {
		if err == sql.ErrNoRows {
			// Handle the case where no rows were found for the given phone
			return nil, fmt.Errorf("user not found with phone %s", phone)
		}
		// Handle other errors
		return nil, err
	}
	return &user, nil
}

func (repo *UsersRepository) GetUserByIDDetailRepository(id string) (*userModel.UsersModel, error) {
	user := userModel.UsersModel{}
	err := repo.db.DB.Get(&user, usersQueries.SelectByIDDetail, id)
	if err != nil {
		if err == sql.ErrNoRows {
			// Handle the case where no rows were found for the given id
			return nil, fmt.Errorf("user not found with id %s", id)
		}
		// Handle other errors
		return nil, err
	}
	return &user, nil
}

func (repo *UsersRepository) GetUserByIDUsersDetailRepository(id string) (*userModel.UsersDetailsModel, error) {
	user := userModel.UsersDetailsModel{}
	err := repo.db.DB.Get(&user, usersQueries.SelectByIDUsersDetail, id)
	if err != nil {
		if err == sql.ErrNoRows {
			// Handle the case where no rows were found for the given id
			return nil, fmt.Errorf("user not found with id %s", id)
		}
		// Handle other errors
		return nil, err
	}
	return &user, nil
}

func (repo *UsersRepository) CreateUsersRepository(user *userModel.UsersModel, userDetails *userModel.UsersDetailsModel) error {
	// Open a new transaction
	tx, err := repo.db.BeginTxx(context.Background(), nil)
	if err != nil {
		return err
	}

	defer func() {
		if err != nil {
			tx.Rollback()
			return
		}
		err = tx.Commit()
	}()

	_, err = tx.Exec(usersQueries.InsertUsers, user.ID, user.Email, user.Phone, user.Password, user.Status, user.Type, user.Active, user.CreatedAt)

	if err != nil {
		return err
	}

	// // Execute INSERT query for users details table
	_, err = tx.Exec(usersQueries.InsertUsersDetails, userDetails.ID, userDetails.UsersID, userDetails.FName, userDetails.LName, userDetails.Dob, userDetails.Status, userDetails.Type, userDetails.Active, userDetails.CreatedAt)
	if err != nil {
		return err
	}

	return err
}

func (repo *UsersRepository) UpdateUsersRepository(user *userModel.UsersModel, userDetails *userModel.UsersDetailsModel) error {
	return nil
}

func (repo *UsersRepository) DeleteUsersRepository(user *userModel.UsersModel, userDetails *userModel.UsersDetailsModel) error {
	return nil
}
