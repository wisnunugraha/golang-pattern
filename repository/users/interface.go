package users

import (
	userModel "api-users/app/users/model"
)

type UsersRepositoryInterface interface {
	GetUserByEmailRepository(email string) (*userModel.UsersModel, error)
	GetUserByPhoneRepository(phone string) (*userModel.UsersModel, error)
	GetUserByIDDetailRepository(id string) (*userModel.UsersModel, error)
	GetUserByIDUsersDetailRepository(id string) (*userModel.UsersDetailsModel, error)

	CreateUsersRepository(user *userModel.UsersModel, detail *userModel.UsersDetailsModel) error
	UpdateUsersRepository(user *userModel.UsersModel, detail *userModel.UsersDetailsModel) error
	DeleteUsersRepository(user *userModel.UsersModel, detail *userModel.UsersDetailsModel) error
}
