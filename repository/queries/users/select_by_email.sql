SELECT
  u.id,
  u.email,
  u.phone,
  u.password
FROM
  "users" as u
WHERE
  u.deleted_at IS NULL
  AND u.active = true
  AND u.email = $1;