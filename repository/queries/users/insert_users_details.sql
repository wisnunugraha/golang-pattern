INSERT INTO
  "users_details" (
    id,
    users_id,
    fname,
    lname,
    dob,
    status,
    type,
    active,
    created_at
  )
VALUES
($1,$2,$3,$4,$5,$6,$7,$8,$9);