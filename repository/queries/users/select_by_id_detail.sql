SELECT
  ud.id,
  ud.users_id,
  ud.fname,
  ud.lname,
  ud.dob,
  ud.type,
  ud.active,
  ud.created_at,
  ud.updated_at,
  ud.deleted_at
FROM
  "users_details" AS ud
WHERE 
  ud.deleted_at IS NULL
  AND ud.active = true
  AND ud.users_id = $1;