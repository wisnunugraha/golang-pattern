INSERT INTO
  "users" (
    id,
    email,
    phone,
    password,
    status,
    type,
    active,
    created_at
  )
VALUES
  ($1,$2,$3,$4,$5,$6,$7,$8);