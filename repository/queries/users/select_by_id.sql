SELECT
  u.id,
  u.email,
  u.phone,
  u.type,
  u.status,
  u.active,
  u.created_at,
  u.updated_at,
  u.deleted_at
FROM
  "users" AS u
WHERE
  u.deleted_at IS NULL
  AND u.active = true
  AND u.id = $1;