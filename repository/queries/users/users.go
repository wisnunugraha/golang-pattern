package users

import _ "embed"

var (
	//go:embed insert_users.sql
	InsertUsers string
	//go:embed insert_users_details.sql
	InsertUsersDetails string
	//go:embed select_by_email.sql
	SelectByEmailSecret string
	//go:embed select_by_phone.sql
	SelectByPhoneSecret string
	//go:embed select_by_id.sql
	SelectByIDDetail string
	//go:embed select_by_id_detail.sql
	SelectByIDUsersDetail string
)
