package router

import (
	"api-users/app/auths/controller"

	"github.com/gofiber/fiber/v2"
)

func AuthRouter(apiV1 *fiber.Group) {
	route := apiV1.Group("/auth")
	route.Post("/login", controller.AuthLoginController)
	route.Post("/register", controller.AuthRegisterController)
	// route.Get("/token", controller)
}
