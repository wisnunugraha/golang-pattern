
## Authors

- [@wisnunugraha](https://www.gitlab.com/wisnunugraha)


## Tech Stack

**Server:** Golang, Gofiber

**Package:** Sql, Sqlx, Validator v10, JWT, pq

**Databse:** Postgresql

## Patterns Golang

I made this pattern so that it is easy for me and my team to understand so that we can be more efficient in every product we work on.

## Explanation
```
Makefile
├── app  - Services
│   ├── auths - Services Auth
│   │   ├── controller - Controller Auth
│   │   │   └── auth.go
│   │   ├── dto - Data Transfer Object Auth
│   │   │   └── auth.go
│   │   ├── model - Model Auth
│   │   │   └── auth.go
│   │   └── service - Service Logic Auth
│   │       └── auth.go
│   └── users
│       ├── controller
│       ├── dto
│       │   └── pagination.go
│       ├── model
│       │   ├── customs.go
│       │   └── users.go
│       └── service
├── config - Configurations
│   ├── app.go
│   ├── config.go
│   └── db.go
├── go.mod
├── go.sum
├── main.go
├── pkg - Package of what we will use
│   ├── db
│   │   └── postgres.go
│   ├── log
│   └── middleware - Middleware of Service
│       ├── jwt.go
│       └── jwtAdmins.go
├── readme.md
├── repository - Repository is connecting with Database
│   ├── queries - Query of script sql
│   │   └── users
│   │       ├── insert_users.sql
│   │       ├── insert_users_details.sql
│   │       ├── select_by_email.sql
│   │       ├── select_by_id.sql
│   │       ├── select_by_id_detail.sql
│   │       ├── select_by_phone.sql
│   │       └── users.go - Variable embed from SQL to String
│   └── users - Repository Users
│       ├── interface.go  - Repository Interface Users
│       └── users.go - Repository functions of users
├── router - Router of endpoint
│   └── auth.go
├── server
│   └── server.go - Configuration Server
├── test
└── utils
    ├── enum - Constan Data
    │   └── enum.go 
    ├── helpers - Helper Data
    │   ├── converts.go 
    │   ├── pagination.go
    │   └── times.go
    └── response - Response of return (Error, Validation, Success)
        └── response.go
```
## Usage/Examples

Clone this repository
```
git clone git@gitlab.com:wisnunugraha/golang-pattern.git

cd golang-pattern
```

Install Package
```
go mod install
```

Copy Env and set whit you're database
```
cp .env.example .env
```

Run with Air
```
Air
```

Run with go
```
go run main.go
```
## License

[MIT](https://choosealicense.com/licenses/mit/)