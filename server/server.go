package server

import (
	"log"

	"api-users/config"
	"api-users/pkg/db"
	"api-users/router"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/compress"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/healthcheck"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/monitor"
)

func Server() {

	// connect to DB
	if err := db.Connects(); err != nil {
		log.Panicf("failed database setup. error: %v", err)
	}
	db.Connects()
	// Start the fiber server
	fiberConfig := config.ConfigsFiber()
	app := fiber.New(fiberConfig)
	// Cors Here
	app.Use(cors.New())
	app.Use(healthcheck.New(healthcheck.Config{
		LivenessProbe: func(c *fiber.Ctx) bool {
			return true
		},
		LivenessEndpoint:  "/api/health/live",
		ReadinessEndpoint: "/api/health/ready",
	}))
	// Compress here
	app.Use(compress.New(compress.Config{
		Level: compress.LevelBestSpeed, // 1
	}))
	// Metrics here
	app.Get("/api/metrics/live", monitor.New())
	// Logger Here
	app.Use(logger.New())
	apiV1 := app.Group("/api/v1")
	// Use the AuthRouter function to define routes for the /api/v1 group
	router.AuthRouter(apiV1.(*fiber.Group))
	// router.UsersRouter(apiV1.(*fiber.Group))
	// router.MastersRouter(apiV1.(*fiber.Group))
	defer db.StartDB().Close()
	app.Listen(":3000")
}
