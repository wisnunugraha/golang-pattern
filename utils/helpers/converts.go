package helpers

func GetValueOrDefault(value *string) string {
	if value != nil {
		return *value
	}
	return ""
}

func GetValueOrDefaultInt64(value *int64) int64 {
	if value != nil {
		return *value
	}
	return 0
}

func GetValueOrDefaultInt(value *int) int {
	if value != nil {
		return *value
	}
	return 0
}
