package helpers

import "time"

func TimeFormat(date string) time.Time {
	timeNow := time.Now()
	formattedTime := timeNow.Format(date)
	parsedTime, err := time.Parse(date, formattedTime)
	if err != nil {
		return time.Time{}
	}
	return parsedTime
}
