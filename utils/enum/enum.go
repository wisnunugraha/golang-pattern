package enum

import "sync"

type EnumMasterStatusOpname int

const (
	Created EnumMasterStatusOpname = iota + 1
	InProgress
	Submited
	Adjusted
	Obselete
)

var (
	enumMasterStatusOpname = map[int]string{
		int(1): "created",
		int(2): "in progress",
		int(3): "submited",
		int(4): "adjusted",
		int(5): "obselete",
	}
	enumMasterStatusOpnameName = map[string]int{
		"created":     int(1),
		"in progress": int(2),
		"submited":    int(3),
		"adjusted":    int(4),
		"obselete":    int(5),
	}
)

var mutex sync.Mutex

func GetEnumMasterStatusOpname() map[int]string {
	mutex.Lock()
	defer mutex.Unlock()
	return enumMasterStatusOpname
}

func GetEnumMasterStatusOpnameName() map[string]int {
	mutex.Lock()
	defer mutex.Unlock()
	return enumMasterStatusOpnameName
}

// func main() {
// 	// Example usage
// 	statusOpname := GetEnumMasterStatusOpname()
// 	statusOpnameName := GetEnumMasterStatusOpnameName()

// 	println(statusOpname[1])             // Output: created
// 	println(statusOpnameName["created"]) // Output: 1
// }
