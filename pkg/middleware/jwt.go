package middleware

import (
	"api-users/config"
	"errors"
	"fmt"
	"net/http"
	"strings"

	"github.com/gofiber/fiber/v2"
	jwtware "github.com/gofiber/jwt/v3"
	"github.com/golang-jwt/jwt"
)

func Protected() fiber.Handler {
	jwtSecretKey := config.ConfigApp().JwtSecretKey
	if jwtSecretKey == "" {
		// Handle empty JWT secret key error
		panic("JWT secret key is empty")
	}
	// Convert jwtSecretKey to []byte
	secretKeyBytes := []byte(jwtSecretKey)
	// Create JWT middleware
	jwtMiddleware := jwtware.New(jwtware.Config{
		SigningKey:     secretKeyBytes,
		SuccessHandler: jwtSuccess,
		ErrorHandler:   jwtError,
	})

	return jwtMiddleware
}

func jwtSuccess(c *fiber.Ctx) error {
	jwtSecretKey := config.ConfigApp().JwtSecretKey
	authHeader := c.Get("Authorization")
	if authHeader == "" {
		return c.Status(http.StatusUnauthorized).JSON(fiber.Map{"error": "Missing Authorization header"})
	}

	// Extract the JWT token from the Authorization header
	parts := strings.Split(authHeader, " ")
	if len(parts) != 2 || parts[0] != "Bearer" {
		return c.Status(http.StatusUnauthorized).JSON(fiber.Map{"error": "Invalid Authorization header format"})
	}
	tokenString := parts[1]

	// Parse and validate the JWT token
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		// Check the signing method
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		// Provide the signing key here
		return []byte(jwtSecretKey), nil
	})
	if err != nil || !token.Valid {
		return c.Status(http.StatusUnauthorized).JSON(fiber.Map{"error": "Invalid token"})
	}

	// Token is valid, extract user data from the token claims
	_, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return c.Status(http.StatusUnauthorized).JSON(fiber.Map{"error": "Invalid token claims"})
	}
	// Now you can access user data from the claims
	// userID := claims["users_id"].(string)
	// c.QueryParser(&userID)
	// c.BodyParser(&userID)
	return c.Next()
}

func jwtError(c *fiber.Ctx, err error) error {
	if err.Error() == "Missing or malformed JWT" {
		c.SendStatus(http.StatusUnauthorized)
		return c.JSON(fiber.Map{"error": "Missing or malformed JWT"})
	}

	c.SendStatus(http.StatusForbidden)
	return c.JSON(fiber.Map{"error": "Invalid or expired token"})
}

func JwtParser(c *fiber.Ctx) (map[string]interface{}, error) {
	jwtSecretKey := config.ConfigApp().JwtSecretKey
	authHeader := c.Get("Authorization")
	if authHeader == "" {
		return nil, errors.New("missing Authorization header")
	}

	// Extract the JWT token from the Authorization header
	parts := strings.Split(authHeader, " ")
	if len(parts) != 2 || parts[0] != "Bearer" {
		return nil, errors.New("invalid Authorization header format")
	}
	tokenString := parts[1]

	// Parse and validate the JWT token
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		// Check the signing method
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		// Provide the signing key here
		return []byte(jwtSecretKey), nil
	})
	if err != nil || !token.Valid {
		return nil, errors.New("invalid token")
	}

	// Token is valid, extract user data from the token claims
	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return nil, errors.New("invalid token claims")
	}
	// Now you can access user data from the claims
	userID := claims["users_id"].(string)
	permission := claims["admin"].(bool)
	response := fiber.Map{
		"users_id":   userID,
		"permission": permission,
	}

	return response, nil
}
